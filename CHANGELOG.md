# CHANGELOG
[1.0.0](https://gitlab.com/alt-cicd/goals/release-tagging/-/tags/1.0.0) - 2023-11-13
===================

### Added
* Initial alt-cicd/goals/release-tagging module
