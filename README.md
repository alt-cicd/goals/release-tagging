<img src="docs/images/alt-logo.png" alt="alt-cicd" width="300"/>

<a name="heading">alt-cicd module: goals/release-tagging</a>
==============================================

<a name="titlebar">Introduction</a>
---------------------------

The **alt-cicd/goals/release-tagging**  module provides the version (pre-release and final) tagging jobs, using
the alt-cicd/tools/git module.

## <a name="contents">Contents</a>
1. [Variables](#variables)
4. [Templates](#templates)
   1. [Variables](#template-variables)
   2. [Rules](#template-rules)
   3. [Scripts](#template-scripts)
   1. [Jobs](#template-jobs)
5. [Jobs](#jobs)
   1. [git_tag_prerelease](#git_tag_prerelease)
   1. [git_tag_prerelease_with_author](#git_tag_prerelease_with_author)
   1. [git_tag_prerelease_with_user_id](#git_tag_prerelease_with_user_id)
   1. [git_tag_prerelease_with_user_login](#git_tag_prerelease_with_user_login)
   1. [git_tag_prerelease_with_commit_ref](#git_tag_prerelease_with_commit_ref)
   1. [git_tag_prerelease_with_protected_ref](#git_tag_prerelease_with_protected_ref)
   1. [git_tag_release](#git_tag_release)
   1. [git_tag_release_as_latest](#git_tag_release_as_latest)


<a name="variables">Variables</a>
========================================

All CICD variables are documented in-line.  The current set of release goal variables is:

```yaml
variables:
   CICD_MODULE_GOALS_RELEASE_TAGGING: 'true'                                               # Identifies that the goals-release-tagging module is included / active.
   CICD_MODULE_GOALS_RELEASE_TAGGING_VERSION: '2.2.0'                                      # Identifies the goals-release-tagging  module version
   CICD_TAG_PRERELEASE_CHANGESET_ENABLED: 'false'                                          # Tag of prerelease with changeset job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_CHANGESET_WITH_AUTHOR_ENABLED: 'false'                              # Tag of prerelease with changeset plus author job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_CHANGESET_WITH_USER_ENABLED: 'false'                                # Tag of prerelease with changeset plus user job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_CHANGESET_WITH_BUILD_ENABLED: 'false'                               # Tag of prerelease with changeset plus build job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_CHANGESET_WITH_COMMIT_REF_ENABLED: 'false'                          # Tag of prerelease with changeset plus commit_ref job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_CHANGESET_WITH_COMMIT_SHA_ENABLED: 'false'                          # Tag of prerelease with changeset plus commit_sha job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_CHANGESET_WITH_PROTECTED_REF_ENABLED: 'false'                       # Tag of prerelease with changeset plus protected commit_ref job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_CHANGESET_WITH_SHA256SUM_ENABLED: 'false'                           # Tag of prerelease with changeset plus sha256sum job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_ENABLED: 'false'                                                    # Enables prerelease tagging if set to 'true' (see above variants)
   CICD_TAG_PRERELEASE_GOAL_PATTERN: '/^pre-release/'                                      # Enables prerelease tagging in the matching goal, if enabled
   CICD_TAG_PRERELEASE_PREFIX: ''                                                          # Prefix to use when tagging prereleases, e.g. 'develop/'
   CICD_TAG_PRERELEASE_WITH_AUTHOR_ENABLED: 'false'                                        # Tag of prerelease with author job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_AUTHOR_PATTERN: '/.*/'                                         # Tag of prerelease with commit_ref job will run if CI_COMMIT_AUTHOR matches, if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_USER_ENABLED: 'false'                                          # Tag of prerelease with author job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_USER_PATTERN: '/.*/'                                           # Tag of prerelease with commit_ref job will run if GITLAB_USER_ID or GITLAB_USER_LOGIN matches, if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_BUILD_ENABLED: 'false'                                         # Tag of prerelease with build job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_COMMIT_REF_ENABLED: 'false'                                    # Tag of prerelease with commit_ref job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_COMMIT_REF_PATTERN: '/^(main|master)$/'                        # Tag of prerelease with commit_ref job will run if CI_COMMIT_REF_NAME matches, if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_COMMIT_SHA_ENABLED: 'false'                                    # Tag of prerelease with commit_sha job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_PROTECTED_REF_ENABLED: 'false'                                 # Tag of prerelease with protected commit_ref job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_PRERELEASE_WITH_SHA256SUM_ENABLED: 'false'                                     # Tag of prerelease with sha256sum job will run if  set to 'true', if prerelease tagging enabled
   CICD_TAG_RELEASE_ENABLED: 'true'                                                        # Enables final release tagging if set to 'true' (see above variants)
   CICD_TAG_RELEASE_GOAL_PATTERN: '/^(final-release)$/'                                    # Enables release tagging in the matching goal, if enabled
   CICD_TAG_RELEASE_WITH_COMMIT_REF_ENABLED: 'false'                                       # Enables release tagging on a branch
   CICD_TAG_RELEASE_WITH_COMMIT_REF_PATTERN: '/^(main|master)/'                            # Restricts release tagging to main or master CI_DEFAULT_BRANCH, if enabled
   CICD_TAG_RELEASE_LATEST_ENABLED: 'false'                                                # Enables tagging final release as "latest" from CICD_TAG_RELEASE_LATEST
   CICD_TAG_RELEASE_LATEST: 'latest'                                                       # Value tagged for latest stable (final) release
```

<a name="templates">Templates</a>
======================================

## <a name="template-variables">Variables</a>

The module contains a matching variables template, including the name of the module, which jobs in the module
extend:

```yaml
#----------------------------------------------------------
# Templates - Variables
#----------------------------------------------------------

.goals_release_tagging:                                                                            # Module specific variable over-rides, referenced in module jobs
   variables:
      CICD_MODULE_NAME: 'goals-release-tagging'
      CICD_MODULE_VERSION: $CICD_MODULE_GOALS_RELEASE_TAGGING_VERSION
```
## <a name="template-rules">Rules</a>

None defined.

##<a name="template-scripts">Script</a>

None defined.

## <a name="template-jobs">Jobs</a>

None defined.

<a name="jobs">Jobs</a>
======================================

The goals-release module defines the following tagging jobs, to tag releases with various metadata (or none for final).

## <a name="git_tag_prerelease">git_tag_prerelease</a>

The tag_prerelease job tracks and tags the CICD_PROJECT_VERSION semantic version value, and appends useful
build metadata when enabled for defined branch name patterns.

It's useful for tracking latest development, ahead of the default.

For example, it can be used with the CICD project to enable inclusion of working builds; the following
example includes the latest unstable commit of the CICD project, pinned at `1.0.0-changeset`:

## <a name="git_tag_prerelease_with_author">git_tag_prerelease_with_author</a>

The tag_prerelease_with_author job tracks and tags the CICD_PROJECT_VERSION semantic version value, and appends useful
build metatdata when enabled for defined branch name patterns.

It's useful for tracking project and release dependencies, for a given user.

For example, it can be used with the CICD project to enable inclusion of working builds; the following
example includes the latest commit/push by Jane on the CICD project, pinned at `1.0.0-changeset+doej`:

## <a name="git_tag_prerelease_with_user_login">git_tag_prerelease_with_user_login</a>

The tag_prerelease_with_author job tracks and tags the CICD_PROJECT_VERSION semantic version value, and appends useful
build metatdata when enabled for defined branch name patterns.

It's useful for tracking project and release dependencies, for a given user.

For example, it can be used with the CICD project to enable inclusion of working builds; the following
example includes the latest commit/push by Jane on the CICD project, pinned at `1.0.0-changeset+doej`:

## <a name="git_tag_prerelease_with_user_id">git_tag_prerelease_with_user_id</a>

The tag_prerelease_with_author job tracks and tags the CICD_PROJECT_VERSION semantic version value, and appends useful
build metatdata when enabled for defined branch name patterns.

It's useful for tracking project and release dependencies, for a given user.

For example, it can be used with the CICD project to enable inclusion of working builds; the following
example includes the latest commit/push by Jane on the CICD project, pinned at `1.0.0-changeset+userid-41`:

## <a name="git_tag_prerelease_with_commit_ref">git_tag_prerelease_with_commit_ref</a>

The tag_prerelease_with_commit_ref job tracks and tags the CICD_PROJECT_VERSION semantic version value, and appends useful
build metatdata when enabled for defined branch name patterns.

It's useful for tracking project and release dependencies, but may be very noisy (verbose) if enabled for branches other
than the default.

For example, it can be used with the CICD project to enable inclusion of working builds; the following
example includes the latest merge to main of the CICD project, pinned at `1.0.0-changeset+main`:

## <a name="git_tag_prerelease_with_protected_ref">git_tag_prerelease_with_protected_ref</a>

The git_tag_prerelease_with_protected_ref job tracks and tags the CICD_PROJECT_VERSION semantic version value, and
appends protected branch name when enabled for protected branches.

It's useful for tracking project and release dependencies, but may be very noisy (verbose) if enabled for branches other
than the default.

For example, it can be used with the CICD project to enable inclusion of working builds; the following
example includes the latest merge to main of the CICD project, pinned at `1.0.0-changeset+main`:

## <a name="git_tag_release">git_tag_release</a>

The tag_release job tracks and tags the CICD_PROJECT_VERSION semantic version value, and appends no
build metatdata.

## <a name="git_tag_release">git_tag_release_as_latest</a>

The tag_release_as_latest job tracks and tags the latest final version (matching the sha of the final release)